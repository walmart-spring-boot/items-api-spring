package com.classpath.items.model;

import lombok.*;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "itemId")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Item {

    private long itemId;

    private double itemPrice;

    private String name;

    private String desc;
}