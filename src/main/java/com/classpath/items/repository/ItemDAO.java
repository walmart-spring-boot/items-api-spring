package com.classpath.items.repository;

import com.classpath.items.model.Item;

import java.util.Set;

public interface ItemDAO {
    public Item save(Item item);
    public Set<Item> fetchItems();
    public void deleteItem(long itemId);
    public Item update(long itemId, Item item);

    Item fetchItemById(long itemId);
}