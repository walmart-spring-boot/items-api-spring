package com.classpath.items.repository;

import com.classpath.items.model.Item;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public class ItemDAOImpl implements ItemDAO {
    @Override
    public Item save(Item item) {
        System.out.println(" Inside the save method of ItemDAOImpl::::");
        return item;
    }

    @Override
    public Set<Item> fetchItems() {
        return null;
    }

    @Override
    public void deleteItem(long itemId) {

    }

    @Override
    public Item update(long itemId, Item item) {
        return null;
    }

    @Override
    public Item fetchItemById(long itemId) {
        return null;
    }
}