package com.classpath.items.service;

import com.classpath.items.model.Item;

import java.util.Set;

public interface ItemService {

    public Item saveItem(Item item);

    public Set<Item> fetchItems ();

    public Item fetchItemById(long itemId);

    public void deleteItemById(long itemId);
}