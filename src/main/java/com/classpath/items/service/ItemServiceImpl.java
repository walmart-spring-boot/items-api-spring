package com.classpath.items.service;

import com.classpath.items.model.Item;
import com.classpath.items.repository.ItemDAO;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service("itemService")
@AllArgsConstructor
public class ItemServiceImpl implements ItemService {

    private ItemDAO itemDAO;

/*
    public ItemServiceImpl(ItemDAO itemDAO){
        this.itemDAO = itemDAO;
    }*/

    @Override
    public Item saveItem(Item item) {
        return this.itemDAO.save(item);
    }

    @Override
    public Set<Item> fetchItems() {
        return this.itemDAO.fetchItems();
    }

    @Override
    public Item fetchItemById(long itemId) {
        return this.itemDAO.fetchItemById(itemId);
    }

    @Override
    public void deleteItemById(long itemId) {
        this.itemDAO.deleteItem(itemId);
    }
}